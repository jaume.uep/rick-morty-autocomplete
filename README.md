# Rick & Morty autocomplete

This Vuejs component implements a input with autocomplete fed by [rickandmortyapi.com](https://rickandmortyapi.com/) REST API.

## Prerequisites

Before you begin, ensure you have met the following requirements:

* You have installed the latest version of `Node.js`
* You have installed the latest version of `npm`

## Installation

To install this component, follow these steps:

1. Install the `Gitlab` package with `$ npm install --save gitlab:jaume.uep/rick-morty-autocomplete`

## Basic Usage

```
<template>
    <rick-morty-autocomplete
        label="Encuentra profesionales de confianza"
        placeholder="Qué necesitas..."
    />
</template>

<script>
import RickMortyAutocomplete from "./components/RickMortyAutocomplete";
export default {
  name: "app",
  components: {
    RickMortyAutocomplete,
  },
};
</script>
```

## Demo
To see the available demo, follow these steps:

1. Clone the `Gitlab` package with `$ git clone https://gitlab.com/jaume.uep/rick-morty-autocomplete.git`
2. Download all `npm` dependencies with `$ npm install`
3. Serve the demo with `$ npm run serve`


## Fonts

Avenir Next fonts where found at [bestfonts.pro](https://es.bestfonts.pro/font/avenir-next)

## Contributing to LootApp
To contribute to Rick & Morty autocomplete, follow these steps:

1. Fork this repository.
2. Create a branch: `git checkout -b <branch_name>`.
3. Make your changes and commit them: `git commit -m '<commit_message>'`
4. Push to the original branch: `git push origin <project_name>/<location>`
5. Create the pull request.


## Contributors

Thanks to the following people who have contributed to this project:

* [@jaume.uep](https://gitlab.com/jaume.uep)

## Contact

If you want to contact me you can reach me at <jaume.uep@gmail.com>.
